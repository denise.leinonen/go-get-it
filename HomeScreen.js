import {View, TouchableOpacity, Text} from 'react-native';
import React, {useCallback, useEffect, useState} from "react";
import { FontAwesome } from '@expo/vector-icons';
import {Styling} from "./Styling";

const names = [
    'Scan to find your dream outfit', 'Outfits around the world...', 'Find out more!'
]

export default function HomeScreen({ navigation }) {

    const [newName, setName] = useState("");

    const shuffle = useCallback(() => {
        const index = Math.floor(Math.random() * names.length);
        setName(names[index]);
    }, []);

    useEffect(() => {
        const intervalID = setInterval(shuffle, 3000);
        return () => clearInterval(intervalID);
    }, [shuffle])

    return (
        <View style={Styling.container}>
            <Text style={Styling.textStyle}>{newName}</Text>
            <View style={Styling.buttonContainer}>
                <TouchableOpacity onPress ={() => navigation.navigate('CameraFunction')}>
                    <FontAwesome name="camera" size={100} color="#FFB6C1" />
                </TouchableOpacity>
                <TouchableOpacity onPress ={() => navigation.navigate('Images')}>
                    <FontAwesome name="photo" size={100} color="#FFB6C1" />
                </TouchableOpacity>
            </View>
        </View>
    );
}
