import React from "react";
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import CameraPage from "./Camera/CameraPage";
import HomeScreen from "./HomeScreen";
import CameraRoll from "./Camera/CameraRoll";

const Routes = createStackNavigator();

export default function App() {

    return (
        <NavigationContainer>
            <Routes.Navigator>
                <Routes.Screen
                    name="Home"
                    component={HomeScreen} />
                <Routes.Screen
                    name="CameraFunction"
                    component={CameraPage}
                />
                <Routes.Screen
                    name="Images"
                    component={CameraRoll}
                />
            </Routes.Navigator>
        </NavigationContainer>
    );
}

