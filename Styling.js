import {StyleSheet} from "react-native";

export const Styling = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFDBE9'
    },
    buttonContainer: {
        backgroundColor: 'transparent',
        justifyContent: 'space-between',
    },
    textStyle: {
        color: "#FFB6C1",
        fontSize: 20,
        marginBottom: '5%',
        fontWeight: 'bold',
        textAlign: 'center'
    }
})
